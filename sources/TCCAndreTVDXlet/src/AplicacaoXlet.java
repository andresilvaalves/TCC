

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.andre.tcc.core.webservice.Aviso;
import org.andre.tcc.core.webservice.EnumAviso;
import org.andre.tcc.core.webservice.WSTCCAndre;
import org.andre.tcc.core.webservice.WSTCCAndreServiceLocator;
import org.andre.tcc.jpa.enumeration.EnumTempo;
import org.havi.ui.HDefaultTextLayoutManager;
import org.havi.ui.HScene;
import org.havi.ui.HSceneFactory;
import org.havi.ui.HScreen;
import org.havi.ui.HStaticIcon;
import org.havi.ui.HStaticText;

import xjavax.tv.xlet.Xlet;
import xjavax.tv.xlet.XletContext;
import xjavax.tv.xlet.XletStateChangeException;

public class AplicacaoXlet implements Xlet, KeyListener{

	private XletContext context;
	private HScene scene;
	private HStaticText textoInicial, lbAviso, lbTemperaturaMinima,lbTemperaturaMaxima, lbMensagem;
	private HStaticIcon imagem;
	
	public AplicacaoXlet() {
	}
	
	@Override
	public void initXlet(XletContext ctx) throws XletStateChangeException {
		this.context = ctx;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int codigo = e.getKeyCode();
		switch (codigo) {
		case 37://seta esquerda
			break;
		case 39: //seta direita
			break;
		case 403://vermelho
			buscaAvisoPeloWS();
			break;
		case 404: //verde
			break;
		case 405://amarelo
			break;
		case 406://azul
			break;
		default:
			break;
		}
		
		//textoInicial = new HStaticText(mensagem,35,45,600,100,new Font("Tiresias",1,20),Color.RED,Color.WHITE, new HDefaultTextLayoutManager());
		
	}

	/**
	 * Metodo que ir� chamar o WS e atualizar a tela do Xlet
	 */
	private void buscaAvisoPeloWS() {
		org.andre.tcc.core.webservice.Aviso aviso = null;
		try {
			WSTCCAndreServiceLocator locator = new WSTCCAndreServiceLocator();
			WSTCCAndre wstccAndre = locator.getWSTCCAndrePort();
			//chama metodo do WS
			aviso = wstccAndre.solicitaDadosLocalizacaoSemParametros();
			// Atualiza tela
			String msgMaxima = "M�xima: " + aviso.getMeteorologia().getTemperaturaMaxima();
			String msgMinima = "M�nima: " + aviso.getMeteorologia().getTemperaturaMinima();
			String msgAviso = aviso.getAviso().getValue();
			String msgMensagemAviso = aviso.getMensagem();
			lbTemperaturaMaxima = new HStaticText(msgMaxima, 35, 125, 130, 40, new Font("Tiresias", 1, 20), Color.RED, Color.WHITE, new HDefaultTextLayoutManager());
			lbTemperaturaMinima = new HStaticText(msgMinima, 35, 160, 130, 40, new Font("Tiresias", 1, 20), Color.RED, Color.WHITE, new HDefaultTextLayoutManager());
			lbAviso = new HStaticText(msgAviso, 35, 200, 130, 40, new Font("Tiresias", 1, 20), Color.RED, Color.WHITE, new HDefaultTextLayoutManager());
			lbMensagem = new HStaticText(msgMensagemAviso, 35, 240, 500, 40, new Font("Tiresias", 1, 20), Color.RED, Color.WHITE, new HDefaultTextLayoutManager());
			String nomeImagem = retornaNomeImagem(aviso);
			Image image = Toolkit.getDefaultToolkit().getImage(nomeImagem);
			imagem = new HStaticIcon(image, 35, 10, 120, 120);

			scene.removeAll();
			scene.add(lbTemperaturaMaxima);
			scene.add(lbTemperaturaMinima);
			scene.add(lbMensagem);
			scene.add(lbAviso);
			scene.add(imagem);

			scene.repaint();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	private String retornaNomeImagem(Aviso aviso) {
		String nomeImagem = "";
		if (aviso.getMeteorologia().getTempo().getValue().equals(org.andre.tcc.core.webservice.EnumTempo._SOL)) {
			nomeImagem = "imagem/sunny.png";
		} else if (aviso.getMeteorologia().getTempo().getValue().equals(org.andre.tcc.core.webservice.EnumTempo._CHUVA)) {
			nomeImagem = "imagem/rain.png";
		} else if (aviso.getMeteorologia().getTempo().getValue().equals(org.andre.tcc.core.webservice.EnumTempo._NUBLADO)) {
			nomeImagem = "imagem/mostly_cloudy.png";
		} else if (aviso.getMeteorologia().getTempo().getValue().equals(org.andre.tcc.core.webservice.EnumTempo._TROVOADA)) {
			nomeImagem = "imagem/thunderstorms.png";
		}else{
			nomeImagem = "imagem/clear.png";
		}
		return nomeImagem;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroyXlet(boolean arg0) throws XletStateChangeException {
		if(scene != null){
			scene.setVisible(false);
			scene.removeAll();
			scene = null;
		}
		context.notifyDestroyed();
	}

	@Override
	public void pauseXlet() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startXlet() throws XletStateChangeException {
		HSceneFactory hSceneFactory = HSceneFactory.getInstance();
		scene = hSceneFactory.getFullScreenScene(HScreen.getDefaultHScreen().getDefaultHGraphicsDevice());
		scene.setSize(640,480);
		scene.setLayout(null);
		scene.addKeyListener(this);
		
		String msgMaxima = "M�xima 21�";
		String msgMinima = "M�nima 14�";
		String msgAviso = "Sem Aviso";
		String msgMensagemAviso = "Mensagem gerada pelo CIRAM. N�o existe aviso";
		
		
		lbTemperaturaMaxima = new HStaticText(msgMaxima,35,125,130,40,new Font("Tiresias",1,20),Color.RED,Color.WHITE, new HDefaultTextLayoutManager());
		lbTemperaturaMinima = new HStaticText(msgMinima,35,160,130,40,new Font("Tiresias",1,20),Color.RED,Color.WHITE, new HDefaultTextLayoutManager());
		lbAviso = new HStaticText(msgAviso,35,200,130,40,new Font("Tiresias",1,20),Color.RED,Color.WHITE, new HDefaultTextLayoutManager());
		lbMensagem = new HStaticText(msgMensagemAviso,35,240,500,40,new Font("Tiresias",1,20),Color.RED,Color.WHITE, new HDefaultTextLayoutManager());
		
		Image image = Toolkit.getDefaultToolkit().getImage("imagem/sunny.png");
		imagem = new HStaticIcon(image,35,10,120,120);
		
		scene.add(lbTemperaturaMaxima);
		scene.add(lbTemperaturaMinima);
		scene.add(lbMensagem);
		scene.add(lbAviso);
		scene.add(imagem);
		
		scene.setVisible(true);
		scene.requestFocus();
	}

}
