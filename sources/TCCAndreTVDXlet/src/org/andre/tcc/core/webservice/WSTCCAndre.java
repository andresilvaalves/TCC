/**
 * WSTCCAndre.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.andre.tcc.core.webservice;

public interface WSTCCAndre extends java.rmi.Remote {
    public org.andre.tcc.core.webservice.Aviso solicitaDadosLocalizacao(double latitude, double longitude) throws java.rmi.RemoteException;
    public org.andre.tcc.core.webservice.Aviso solicitaDadosLocalizacaoSemParametros() throws java.rmi.RemoteException;
}
