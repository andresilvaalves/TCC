/**
 * WSTCCAndreService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.andre.tcc.core.webservice;

public interface WSTCCAndreService extends javax.xml.rpc.Service {
    public java.lang.String getWSTCCAndrePortAddress();

    public org.andre.tcc.core.webservice.WSTCCAndre getWSTCCAndrePort() throws javax.xml.rpc.ServiceException;

    public org.andre.tcc.core.webservice.WSTCCAndre getWSTCCAndrePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
