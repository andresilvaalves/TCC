/**
 * WSTCCAndreServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.andre.tcc.core.webservice;

public class WSTCCAndreServiceLocator extends org.apache.axis.client.Service implements org.andre.tcc.core.webservice.WSTCCAndreService {

    public WSTCCAndreServiceLocator() {
    }


    public WSTCCAndreServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WSTCCAndreServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WSTCCAndrePort
    private java.lang.String WSTCCAndrePort_address = "http://andre-note:8082/WSTCCAndreService/WSTCCAndre";

    public java.lang.String getWSTCCAndrePortAddress() {
        return WSTCCAndrePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WSTCCAndrePortWSDDServiceName = "WSTCCAndrePort";

    public java.lang.String getWSTCCAndrePortWSDDServiceName() {
        return WSTCCAndrePortWSDDServiceName;
    }

    public void setWSTCCAndrePortWSDDServiceName(java.lang.String name) {
        WSTCCAndrePortWSDDServiceName = name;
    }

    public org.andre.tcc.core.webservice.WSTCCAndre getWSTCCAndrePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WSTCCAndrePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWSTCCAndrePort(endpoint);
    }

    public org.andre.tcc.core.webservice.WSTCCAndre getWSTCCAndrePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.andre.tcc.core.webservice.WSTCCAndrePortBindingStub _stub = new org.andre.tcc.core.webservice.WSTCCAndrePortBindingStub(portAddress, this);
            _stub.setPortName(getWSTCCAndrePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWSTCCAndrePortEndpointAddress(java.lang.String address) {
        WSTCCAndrePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.andre.tcc.core.webservice.WSTCCAndre.class.isAssignableFrom(serviceEndpointInterface)) {
                org.andre.tcc.core.webservice.WSTCCAndrePortBindingStub _stub = new org.andre.tcc.core.webservice.WSTCCAndrePortBindingStub(new java.net.URL(WSTCCAndrePort_address), this);
                _stub.setPortName(getWSTCCAndrePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WSTCCAndrePort".equals(inputPortName)) {
            return getWSTCCAndrePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "WSTCCAndreService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "WSTCCAndrePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WSTCCAndrePort".equals(portName)) {
            setWSTCCAndrePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
