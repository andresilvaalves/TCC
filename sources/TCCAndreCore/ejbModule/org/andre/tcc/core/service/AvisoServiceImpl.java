package org.andre.tcc.core.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.andre.tcc.jpa.entity.Aviso;

/**
 * Session Bean implementation class AvisoServiceImpl
 */
@Stateless(mappedName = "ejb/AvisoEJB")
@LocalBean
public class AvisoServiceImpl extends BasePersistenceServiceImpl<Aviso> implements AvisoService {

    /**
     * Default constructor. 
     */
    public AvisoServiceImpl() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Aviso buscaAvisoLocalizacao(double latitude, double longitude) {
		Query query = this.createNamedQuery("buscaAvisoByLocalizacao");
		query.setParameter("latitude", latitude);
		query.setParameter("longitude", longitude);
		List<Aviso> avisos = query.getResultList();
		if(avisos != null && !avisos.isEmpty()){
			return avisos.get(0);
		}
		
	/*	*//**
		 * TODO para testes
		 * Se n�o encontrou a localiza��o exata, ir� buscar todos os registros e retornar o primeiro 
		 *//*
		avisos = this.listAll(Aviso.class);
		if(avisos != null && !avisos.isEmpty()){
			return avisos.get(0);
		}*/
		
		//se n�o encontra retorna null
		return  null;
	}


	@Override
	public int limpaAvisos() {
		Query query = this.createNamedQuery("limpaAvisos");
		int result = query.executeUpdate();
		return result;
	}

	@Override
	public Collection<Aviso> buscaAvisosDiario() {
		Query query = this.createNamedQuery("buscaAvisosDiario");
		//pega o dia atual
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		//busca os avisos pela data atual
		query.setParameter("data", calendar);
		Collection<Aviso> avisos = query.getResultList();
		return avisos;
	}

}
