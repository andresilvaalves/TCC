package org.andre.tcc.web;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import org.andre.tcc.core.bean.TCCAndreEJBLocal;
import org.andre.tcc.jpa.entity.Aviso;

@ManagedBean(value="ejb/TCCAndreBean")
@RequestScoped
public class TCCAndreMB {

	@EJB
	private TCCAndreEJBLocal ejb;
	
	private Aviso aviso = new Aviso();// = new Aviso(-29.020993,-53.564572);
	
	public String solicitaDadosLocalizacao(){
		this.aviso = ejb.solicitaDadosLocalizacao(this.aviso.getLatitude(), this.aviso.getLongitude());
		
		return "mostraDados";
	}


	public Aviso getAviso() {
		return aviso;
	}


	public void setAviso(Aviso aviso) {
		this.aviso = aviso;
	}
	
	
}
