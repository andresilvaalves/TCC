package org.andre.tcc.ciram.exception;

public class CiramException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3137547621769229810L;

	public CiramException(String msgErro, Exception e) {
		super(msgErro, e);
	}
	
}
