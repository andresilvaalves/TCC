package org.andre.tcc.ciram.model;

import org.andre.tcc.ciram.model.enumeration.TipoCTL;

public abstract class PosicionamentoCTL extends AbstractPosicionamentoCTL {

	private double posicao;
	private double distancia;
	
	
	public PosicionamentoCTL(int celulas, TipoCTL tipoCTL, double posicao, double distancia) {
		super(celulas, tipoCTL);
		this.posicao = posicao;
		this.distancia = distancia;
	}
	
	public double getPosicao() {
		return posicao;
	}
	public void setPosicao(double posicao) {
		this.posicao = posicao;
	}
	public double getDistancia() {
		return distancia;
	}
	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
	
}
