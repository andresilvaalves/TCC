package org.andre.tcc.ciram.model;

import java.util.Date;

import org.andre.tcc.ciram.model.enumeration.TipoCTL;

public class TempoCTL extends AbstractPosicionamentoCTL {

	public TempoCTL(int celulas, TipoCTL tipoCTL, Date data) {
		super(celulas, tipoCTL);
		this.data = data;
	}

	private Date data;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	
}
