package org.andre.tcc.ciram.model.enumeration;

public enum TipoCTL {

	LINEAR("linear"), NAO_LINEAR("nao_linear");
	
	private String valor;
	
	private TipoCTL(String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return valor;
	}
	
}
