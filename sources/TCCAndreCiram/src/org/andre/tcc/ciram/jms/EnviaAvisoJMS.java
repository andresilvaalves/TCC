package org.andre.tcc.ciram.jms;

import java.util.Collection;
import java.util.Properties;

import javax.interceptor.Interceptors;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.andre.tcc.ciram.exception.CiramException;
import org.andre.tcc.ciram.interceptor.CiramInterceptor;
import org.andre.tcc.jpa.entity.Aviso;

/**
 * Classe responsavel por enviar as mensagens para a lista JMS
 * @author Andr� Silva Alves
 * E-Mail/MSN/Gtalk: andresilvaalves@gmail.com
 * Skype: andresilvaalves
 *
 * 04/05/2011
 *
 */
@Interceptors(CiramInterceptor.class)
public class EnviaAvisoJMS {

	private static EnviaAvisoJMS instance;
	
	private EnviaAvisoJMS() {
	}
	
	public static EnviaAvisoJMS getInstance(){
		if(instance == null)
			instance = new EnviaAvisoJMS();
		return instance;
	}
	
	
	public void enviaAvisos(Collection<Aviso> avisos) throws CiramException{
		if(avisos != null && !avisos.isEmpty()){
			for (Aviso aviso : avisos) {
				enviaMensagemJMS(null, aviso);
				break;
			}
		}
	}
			
	public void enviaAviso(Aviso aviso) throws CiramException{
		enviaMensagemJMS(null, aviso);
	}
	
	
	
	private void enviaMensagemJMS(InitialContext ctx, Aviso aviso) throws CiramException{
		Connection conn = null;
		Session session = null;
		MessageProducer msgProducer = null;
		try{
			if(ctx == null)
				ctx = new InitialContext();
			ConnectionFactory connFactory = (ConnectionFactory) ctx.lookup("TCCAndreQueueFactory");
			Queue queue = (Queue) ctx.lookup("queue/TCCAndre");
			conn = connFactory.createConnection();
			session = conn.createSession(false,Session.AUTO_ACKNOWLEDGE);
			msgProducer = session.createProducer(queue);
			
			ObjectMessage omsg = session.createObjectMessage();
			omsg.setObject(aviso);
			msgProducer.send(omsg);
			
		}catch (JMSException e) {
			throw new CiramException("Erro ao enviar Aviso para a JMS ", e);
		} catch (NamingException e) {
			throw new CiramException("Erro ao enviar Aviso para a JMS. erro no JNDI ", e);
		}finally{
			System.out.println("Msg Enviada : aviso: " + aviso.getDescricao());
			
			if(msgProducer != null){
				try {
					msgProducer.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(session != null){
				try {
					session.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(conn != null){
				try {
					conn.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	
	
	public void enviaMensagemJMSForaGlassFish(Aviso aviso) throws CiramException {
		try {
			Properties props = new Properties();

			props.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");

			props.setProperty("java.naming.factory.url.pkgs", "com.sun.enterprise.naming");

			props.setProperty("java.naming.factory.state", "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");

			// Adicionar host caso o WebService esteja rodando em outra maquina
			props.setProperty("org.omg.CORBA.ORBInitialHost","localhost");

			// definir a porta ORB para CORBA. default � 3700.
			props.setProperty("org.omg.CORBA.ORBInitialPort", "3700");

			InitialContext ctx = new InitialContext(props);
			enviaMensagemJMS(ctx, aviso);//passa o context externo junto com o Objeto
		} catch (NamingException e) {
			throw new CiramException("Erro ao enviar Aviso para a JMS. erro no JNDI", e);
		}

	}
	
	
	
	
	
	
	
	
}
