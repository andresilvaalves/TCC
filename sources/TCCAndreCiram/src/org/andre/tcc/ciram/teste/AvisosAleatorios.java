package org.andre.tcc.ciram.teste;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import org.andre.tcc.jpa.entity.Aviso;
import org.andre.tcc.jpa.entity.Meteorologia;
import org.andre.tcc.jpa.enumeration.EnumAviso;
import org.andre.tcc.jpa.enumeration.EnumDirecaoVento;
import org.andre.tcc.jpa.enumeration.EnumTempo;
import org.andre.tcc.jpa.enumeration.EnumTipoAviso;

public class AvisosAleatorios {

	
	public static Collection<Aviso> avisosAleatorios(double latitude, double longitude, double distancia){		
		Collection<Aviso> resposta = new ArrayList<Aviso>();
		AvisosAleatorios avisosAleatorios = new AvisosAleatorios();
		final double latitudeFinal = latitude;
		for (double x = 0; x < 118; x++) {
			for (double y = 0; y < 72; y++) {
				int aleatorioAviso =  (int) (5*Math.random());		
				resposta.add(avisosAleatorios.geraAvisoAleatorio(aleatorioAviso, latitude, longitude, distancia));
				latitude+=distancia;
			}
			latitude = latitudeFinal;
			longitude+=distancia;
		}
		return resposta;
	}
	
	
	private Aviso geraAvisoAleatorio(int aleatorioAviso, double latitude, double longitude, double distancia) {
		Aviso aviso = new Aviso();
		aviso.setLatitude(latitude);
		aviso.setDistanciaLatitude(distancia);
		aviso.setLongitude(longitude);
		aviso.setDistanciaLongitude(distancia);
		aviso.setData(Calendar.getInstance());
		
		Meteorologia meteorologia = new Meteorologia();			
		int aleatorioMeteorologia = (int)(2*Math.random());
		
		switch (aleatorioAviso) {		
		case 1:
			aviso.setAviso(EnumAviso.AVISO_BAIXO);
			
			switch (aleatorioMeteorologia) {
			case 0:
				aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MINIMA);
				aviso.setDescricao("Aviso baixo de temperatura minima extrema");
				meteorologia.setVelocidadeVento(70);
				meteorologia.setFenomeno("Chuva em grande Volume");
				meteorologia.setTemperaturaMaxima(11);
				meteorologia.setTemperaturaMinima(5);
				meteorologia.setDirecaoVento(EnumDirecaoVento.NORTE);
				meteorologia.setTempo(EnumTempo.CHUVA);
				break;
			default:
				aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MAXIMA);
				aviso.setDescricao("Aviso baixo de temperatura m�xima extrema");
				meteorologia.setVelocidadeVento(70);
				meteorologia.setFenomeno("Ar seco");
				meteorologia.setTemperaturaMaxima(30);
				meteorologia.setTemperaturaMinima(22);
				meteorologia.setDirecaoVento(EnumDirecaoVento.LESTE);
				meteorologia.setTempo(EnumTempo.SOL);
				break;
			}
			break;
		case 2:
			aviso.setAviso(EnumAviso.AVISO_MEDIO);
			
			switch (aleatorioMeteorologia) {
			case 0:
				aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MINIMA);
				aviso.setDescricao("Aviso M�dio de temperatura minima extrema");
				meteorologia.setVelocidadeVento(70);
				meteorologia.setFenomeno("Chuva em grande Volume");
				meteorologia.setTemperaturaMaxima(11);
				meteorologia.setTemperaturaMinima(-2);
				meteorologia.setDirecaoVento(EnumDirecaoVento.NORTE);
				meteorologia.setTempo(EnumTempo.GEADA);
				break;
			default:
				aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MAXIMA);
				aviso.setDescricao("Aviso medio de temperatura m�xima extrema");
				meteorologia.setVelocidadeVento(70);
				meteorologia.setFenomeno("Ar seco");
				meteorologia.setTemperaturaMaxima(30);
				meteorologia.setTemperaturaMinima(22);
				meteorologia.setDirecaoVento(EnumDirecaoVento.LESTE);
				meteorologia.setTempo(EnumTempo.SOL);
				break;
			}
			break;
		case 3:
			aviso.setAviso(EnumAviso.AVISO_ALTO);
			
			switch (aleatorioMeteorologia) {
			case 0:
				aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MINIMA);
				aviso.setDescricao("Aviso Alto de temperatura minima extrema");
				meteorologia.setVelocidadeVento(70);
				meteorologia.setFenomeno("Risco de cheias");
				meteorologia.setTemperaturaMaxima(12);
				meteorologia.setTemperaturaMinima(5);
				meteorologia.setDirecaoVento(EnumDirecaoVento.SUL);
				meteorologia.setTempo(EnumTempo.TROVOADA);
				break;
			default:
				aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MAXIMA);
				aviso.setDescricao("Aviso Alto de temperatura m�xima extrema");
				meteorologia.setVelocidadeVento(40);
				meteorologia.setFenomeno("Risco de queimadas");
				meteorologia.setTemperaturaMaxima(35);
				meteorologia.setTemperaturaMinima(26);
				meteorologia.setDirecaoVento(EnumDirecaoVento.NORTE);
				meteorologia.setTempo(EnumTempo.SOL);
				break;
			}
			break;
		case 4:
			aviso.setAviso(EnumAviso.AVISO_MUITO_ALTO);
			switch (aleatorioMeteorologia) {
			case 0:
				aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MINIMA);
				aviso.setDescricao("Aviso Muito Alto de temperatura minima extrema");
				meteorologia.setVelocidadeVento(70);
				meteorologia.setFenomeno("Risco de forte Geada");
				meteorologia.setTemperaturaMaxima(8);
				meteorologia.setTemperaturaMinima(-4);
				meteorologia.setDirecaoVento(EnumDirecaoVento.NORTE);
				meteorologia.setTempo(EnumTempo.GEADA);
				break;
			default:
				aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MAXIMA);
				aviso.setDescricao("Aviso Muito Alto de temperatura m�xima extrema");
				meteorologia.setVelocidadeVento(30);
				meteorologia.setFenomeno("Risco de queimadas");
				meteorologia.setTemperaturaMaxima(39);
				meteorologia.setTemperaturaMinima(25);
				meteorologia.setDirecaoVento(EnumDirecaoVento.NORTE);
				meteorologia.setTempo(EnumTempo.SOL);
				break;
			}
			break;
		case 0:
		default:
			aviso.setAviso(EnumAviso.SEM_AVISO);
			aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MAXIMA);
			aviso.setDescricao("N�o h� avisos para essa regi�o");
			
			meteorologia.setVelocidadeVento(30);
			meteorologia.setFenomeno("Sol");
			meteorologia.setTemperaturaMaxima(28);
			meteorologia.setTemperaturaMinima(18);
			meteorologia.setDirecaoVento(EnumDirecaoVento.NORTE);
			meteorologia.setTempo(EnumTempo.SOL);
			break;
		}
		aviso.setMeteorologia(meteorologia);
		aviso.setMensagem("Dados ALEAT�RIOS gerados pelo sistema: \n "+aviso.getDescricao());
		return aviso;
	}

	
	public static void main(String[] args) {
		double longitude = -53.902336;
		double latitude = -29.358757;
		double distancia = 0.048252;
		int linhasLongitude = 118;
		int linhasLatitude = 72;
		Collection<Aviso> avisosAleatorios = AvisosAleatorios.avisosAleatorios(latitude, longitude, distancia);
		System.out.println(avisosAleatorios);
	}
}
