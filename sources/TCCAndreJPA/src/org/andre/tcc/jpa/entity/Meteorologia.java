package org.andre.tcc.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.andre.tcc.jpa.enumeration.EnumDirecaoVento;
import org.andre.tcc.jpa.enumeration.EnumTempo;

/**
 * Entity implementation class for Entity: Meteorologia
 *
 */
@Entity
public class Meteorologia implements IEntidade {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="genMeteorologia")
	private long id;
	private EnumTempo tempo;
	private double temperaturaMaxima;
	private double temperaturaMinima;
	private EnumDirecaoVento direcaoVento;
	private double velocidadeVento;
	private String fenomeno;
	
	private static final long serialVersionUID = 1L;

	public Meteorologia() {
		super();
		this.id = -1;
	}   
	@Override
	public long getId() {
		return this.id;
	}
	@Override
	public void setId(long id) {
		this.id = id;
	}
	public double getTemperaturaMaxima() {
		return this.temperaturaMaxima;
	}

	public void setTemperaturaMaxima(double temperaturaMaxima) {
		this.temperaturaMaxima = temperaturaMaxima;
	}   
	public double getTemperaturaMinima() {
		return this.temperaturaMinima;
	}

	public void setTemperaturaMinima(double temperaturaMinima) {
		this.temperaturaMinima = temperaturaMinima;
	}   
	public EnumDirecaoVento getDirecaoVento() {
		return this.direcaoVento;
	}

	public void setDirecaoVento(EnumDirecaoVento direcaoVento) {
		this.direcaoVento = direcaoVento;
	}   
	public double getVelocidadeVento() {
		return this.velocidadeVento;
	}

	public void setVelocidadeVento(double velocidadeVento) {
		this.velocidadeVento = velocidadeVento;
	}   
	public String getFenomeno() {
		return this.fenomeno;
	}

	public void setFenomeno(String fenomeno) {
		this.fenomeno = fenomeno;
	}
	public EnumTempo getTempo() {
		return tempo;
	}
	public void setTempo(EnumTempo tempo) {
		this.tempo = tempo;
	}
   
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		//sb.append("ID="+this.getId());
		sb.append("Fenomeno= "+this.getFenomeno());
		sb.append("Tempo= "+this.getTempo().getDescricaoTempo());
		sb.append("Temperatura Maxima= "+this.getTemperaturaMaxima());
		sb.append("Temperatura Minina= "+this.getTemperaturaMinima());
		sb.append("Velocidade do Vento= "+this.getVelocidadeVento());
		sb.append("Dire��o do Vento= "+this.getDirecaoVento().getDescricaoDirecaoVento());
		return sb.toString();
	}
}
