package org.andre.tcc.jpa.entity;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.andre.tcc.jpa.enumeration.EnumAviso;
import org.andre.tcc.jpa.enumeration.EnumTipoAviso;


/**
 * 
 * @author Andr� Silva Alves
 * E-Mail/MSN/Gtalk: andresilvaalves@gmail.com
 * Skype: andresilvaalves
 *
 * 17/04/2011
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="buscaAvisoByLocalizacao", 
			query=" select a from Aviso a " +
					" where ( (a.latitude between (:latitude - a.distanciaLatitude) and (:latitude + a.distanciaLatitude) )  " +
					" AND (a.longitude between (:longitude - a.distanciaLongitude) and (:longitude + a.distanciaLongitude) ) )" +
					" OR " +
					" ( (a.latitude between (:latitude + a.distanciaLatitude) and (:latitude - a.distanciaLatitude) )" +
					" AND (a.longitude between (:longitude + a.distanciaLongitude) and (:longitude - a.distanciaLongitude) ) )" ),
	@NamedQuery(name="limpaAvisos", query=" delete from Aviso "),
	@NamedQuery(name="buscaAvisosDiario", query=" select a from Aviso a where a.data > :data")
})
public class Aviso implements IEntidade{
	
	private static final long serialVersionUID = -5449562526130846999L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="genAviso")
	private long id;
	
	private double latitude;
	private double distanciaLatitude;
	private double longitude;
	private double distanciaLongitude;
	private EnumAviso aviso;
	private EnumTipoAviso tipoAviso;
	private String descricao;
	private String mensagem;
	
	@OneToOne(cascade=CascadeType.ALL)
	private Meteorologia meteorologia;
	
	@Temporal(value=TemporalType.TIMESTAMP)
	private Calendar data;

	public Aviso() {
		this.descricao = "";
		this.latitude = 0;
		this.longitude = 0;
		this.mensagem = "";
		this.aviso = EnumAviso.SEM_AVISO;
		this.id = -1;
	}
	
	public Aviso(double latitude, double longitude){
		this();
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	@Override
	public long getId() {
		return this.id;
	}
	@Override
	public void setId(long id) {
		this.id = id;
		
	}

	public double getDistanciaLatitude() {
		return distanciaLatitude;
	}

	public void setDistanciaLatitude(double distanciaLatitude) {
		this.distanciaLatitude = distanciaLatitude;
	}

	public double getDistanciaLongitude() {
		return distanciaLongitude;
	}

	public void setDistanciaLongitude(double distanciaLongitude) {
		this.distanciaLongitude = distanciaLongitude;
	}

	public EnumAviso getAviso() {
		return aviso;
	}
	public void setAviso(EnumAviso aviso) {
		this.aviso = aviso;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Meteorologia getMeteorologia() {
		if(meteorologia == null){
			meteorologia = new Meteorologia();
		}
		return meteorologia;
	}
	public void setMeteorologia(Meteorologia meteorologia) {
		this.meteorologia = meteorologia;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public EnumTipoAviso getTipoAviso() {
		return tipoAviso;
	}

	public void setTipoAviso(EnumTipoAviso tipoAviso) {
		this.tipoAviso = tipoAviso;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		//sb.append("ID="+this.getId());
		sb.append("Aviso="+this.getAviso().getDescricaoAviso());
		sb.append("Tipo Aviso="+this.getTipoAviso().getDescricaoTipoAviso());
		sb.append("Latitude="+this.getLatitude());
		sb.append("Distancia Latitude="+this.getDistanciaLatitude());
		sb.append("Longitude="+this.getLongitude());
		sb.append("Distancia Longitude="+this.getDistanciaLongitude());
		sb.append("Meteorologia="+this.getMeteorologia());
		return sb.toString();
	}
	
}
