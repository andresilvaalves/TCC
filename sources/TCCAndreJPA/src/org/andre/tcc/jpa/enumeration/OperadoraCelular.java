package org.andre.tcc.jpa.enumeration;

public enum OperadoraCelular {

	TIM(1,"TIM"), VIVO(2, "VIVO"), CLARO(3, "CLARO"), OI(4, "OI"), NEXTEL(6, "NEXTEL");
	
	
	private int codigoOperadora;
	private String nomeOperadora;
	
	private  OperadoraCelular(int codigoOperadora, String nomeOperadora){
		this.codigoOperadora = codigoOperadora;
		this.nomeOperadora = nomeOperadora;
	}

	public int getCodigoOperadora() {
		return codigoOperadora;
	}

	public String getNomeOperadora() {
		return nomeOperadora;
	}
	
	
}
