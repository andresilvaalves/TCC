package org.andre.tcc.jpa.enumeration;

public enum EnumTipoAviso {

	TEMPERATURA_MAXIMA(1,"Aviso de Temperatura M�xima Extrema"), 
	TEMPERATURA_MINIMA(2,"Aviso de Temperatura M�nima Extrema"),
	INCENDIO(2,"Avido de Inc�ndio");; 
	
	
	private int codigoTipoAviso;
	private String descricaoTipoAviso;
	
	private  EnumTipoAviso(int codigoTipoAviso, String descricaoAviso){
		this.codigoTipoAviso = codigoTipoAviso;
		this.descricaoTipoAviso = descricaoAviso;
	}

	public int getCodigoTipoAviso() {
		return this.codigoTipoAviso;
	}

	public String getDescricaoTipoAviso() {
		return descricaoTipoAviso;
	}
	
}
