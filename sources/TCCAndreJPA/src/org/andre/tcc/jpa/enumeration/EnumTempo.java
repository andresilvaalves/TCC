package org.andre.tcc.jpa.enumeration;

public enum EnumTempo {

	INDEFINIDO(-1,"Indefinido"),
	SOL(1,"Sol"), 
	NUBLADO(2,"Nublado"), 
	PARCIALMENTE_NUBLADO(3,"Parcialmente Nublado"),
	CHUVA(4,"Chuva"), 
	GEADA(5,"Geada"),	
	TROVOADA(5,"Trovoada");
	
	
	private int codigoTempo;
	private String descricaoTempo;
	
	private  EnumTempo(int codigoTempo, String descricaoTempo){
		this.codigoTempo = codigoTempo;
		this.descricaoTempo = descricaoTempo;
	}

	public int getCodigoTempo() {
		return this.codigoTempo;
	}

	public String getDescricaoTempo() {
		return descricaoTempo;
	}
	
}
